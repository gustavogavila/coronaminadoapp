let pontosTotais = 0;
let divPontuacao;
let resultadoFinal;
let divContainer = document.getElementsByClassName("container")[0];
let tabuleiro;

function montarTabuleiro() {
    let k = 0;
    document.write(`
            <div class='barra'> 
                <span align=left valign=left>
                    <img src="./img/corona-icon.png" valign="left" align="left" width="120" height="120">
                </span>
                <span align=right valign=right>
                    <img src="./img/corona-icon.png" valign="right" align="right" width="120" height="120">
                </span>
                <h1>Corona Minado App</h1>
                
                 
            
                <p>Pontuação: <span class="texto-sucesso" id="pontuacao">0 pontos</span></p>
                <p>Resultado final: <span class="texto-perigo" id="resultadoFinal"></span></p>
                <span id="reiniciar"></span>
            </div>
            <div class="container">
                
            
        `)
    document.write('<table id="tabuleiro">');

    for (var i = 0; i < 10; i++) {
        document.write('<tr>');
        for (var j = 0; j < 10; j++) {
            k = k + 1;
            var num = Math.floor(Math.random() * 4);
            document.write('<td><div class="btn" id="' + k + '" onclick="controle(' + num + ',' + k + ')"><div class="btn-content">CM</div></div></td>');
        }
        document.write('</tr>');

    }
    document.write('</table>');
    document.write(`
        <footer id="reiniciar"></footer>
    `)
    document.write('</container>')
    divPontuacao = document.getElementById("pontuacao");
    resultadoFinal = document.getElementById("resultadoFinal");
    tabuleiro = document.getElementById("tabuleiro");
    divFooter = document.getElementById("reiniciar");
}

function controle(num, k) {
    if (num == 0) {
        var valor = document.getElementById(k);
        valor.getAttribute("id");
        valor.innerText = "";
        valor.appendChild(constroiTagImagem(num));
        gameOver();
    } else {
        var valor = document.getElementById(k);
        valor.getAttribute("id");
        valor.innerText = "";
        valor.appendChild(constroiTagImagem(num));
        atualizaPainelResultados(num);
    }
}

function constroiTagImagem(num) {
    let imagesMap = new Map()
    imagesMap.set(0, './img/corona-icon.png')
    imagesMap.set(1, './img/sabao-icon.png')
    imagesMap.set(2, './img/alcool-icon.png')
    imagesMap.set(3, './img/mascara-icon.png')

    let img = document.createElement("img")
    const imgpath = imagesMap.get(num);
    img.src = imagesMap.get(num);
    img.style.maxWidth = '50%';
    img.style.height = 'auto';
    return img;
}

function atualizaPainelResultados(num) {
    let pontosMap = new Map()
    pontosMap.set(1, 10)
    pontosMap.set(2, 30)
    pontosMap.set(3, 50)
    pontosTotais += pontosMap.get(num);
    divPontuacao.innerHTML = `${pontosTotais} pontos`;
}

function gameOver() {
    resultadoFinal.innerHTML = "Você foi contaminado! :(";
    let botaoReiniciar = document.createElement("input");
    botaoReiniciar.classList.add('btn-reiniciar');
    botaoReiniciar.setAttribute("type", "button");
    botaoReiniciar.setAttribute("value", "Reiniciar o jogo");
    botaoReiniciar.setAttribute("onclick", "reiniciarJogo()");
    divFooter.appendChild(botaoReiniciar);

    botoes = document.querySelectorAll(".btn");
    botoes.forEach(node => {
        node.removeAttribute("onclick");
    })
}

function reiniciarJogo() {
    location.reload();
}

montarTabuleiro();


